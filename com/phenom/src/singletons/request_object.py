from sanic.request import Request

REFNUM: str = "refNum"
DATAFIELDS: str = "dataFields"
JOBSFIElDS: str = "jobsFields"
JOBSLABEL: str = "jobsLabel"
CANDIDATESFIELDS: str = "candidatesFields"
CANDIDATESLABEL: str = "candidatesLabel"


class RequestObject:
    def __init__(self, request):
        self.request = request.json
        if REFNUM in self.request:
            self.refNum: str = self.request[REFNUM]
        if DATAFIELDS in self.request:
            self.dataFields: str = self.request[DATAFIELDS]
        if JOBSFIElDS in self.request:
            self.jobsFields: str = self.request[JOBSFIElDS]
        if JOBSLABEL in self.request:
            self.jobsLabel: str = self.request[JOBSLABEL]
        if CANDIDATESFIELDS in self.request:
            self.candidatesFields: str = self.request[CANDIDATESFIELDS]
        if CANDIDATESLABEL in self.request:
            self.candidatesLabel: str = self.request[CANDIDATESLABEL]

    @property
    def refNum(self) -> str:
        return self.__refNum

    @property
    def dataFields(self):
        return self.__dataFields

    @property
    def jobsFields(self):
        return self.__jobsFields

    @property
    def jobsLabel(self):
        return self.__jobsLabel

    @property
    def candidatesFields(self):
        return self.__candidatesFields

    @property
    def candidatesLabel(self):
        return self.__candidatesLabel

    @refNum.setter
    def refNum(self, value):
        if self.request and REFNUM in self.request:
            self.__refNum = self.request[REFNUM]
        else:
            self.__refNum = value

    @dataFields.setter
    def dataFields(self, value):
        if self.request and DATAFIELDS in self.request:
            self.__dataFields = self.request[DATAFIELDS]
        else:
            self.__dataFields = value

    @jobsFields.setter
    def jobsFields(self, value):
        if self.request and JOBSFIElDS in self.request:
            self.__jobsFields = self.request[JOBSFIElDS]
        else:
            self.__jobsFields = value

    @jobsLabel.setter
    def jobsLabel(self, value):
        if self.request and JOBSLABEL in self.request:
            self.__jobsLabel = self.request[JOBSLABEL]
        else:
            self.__jobsLabel = value

    @candidatesFields.setter
    def candidatesFields(self, value):
        if self.request and CANDIDATESFIELDS in self.request:
            self.__candidatesFields = self.request[CANDIDATESFIELDS]
        else:
            self.__candidatesFields = value

    @candidatesLabel.setter
    def candidatesLabel(self, value):
        if self.request and CANDIDATESLABEL in self.request:
            self.__candidatesLabel = self.request[CANDIDATESLABEL]
        else:
            self.__candidatesLabel = value