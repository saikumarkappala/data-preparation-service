from sanic import Sanic
import time
from sanic.request import Request
from sanic.response import text, json

from com.phenom.src.services.data_cleaner import DataCleaner
from com.phenom.src.singletons.request_object import RequestObject
from com.phenom.src.utils.logging_util import Logging

LOGGER = Logging.get_instance()
app = Sanic.get_app("data_preparation")


class DataPreparationController:
    def __init__(self):
        LOGGER.logger.info("Starting the experience-prediction web service.")

    @app.get("/test")
    async def foo_handler(request: Request) -> str:
        LOGGER.logger.info("recieved testing request")
        return text("Service is up and running")


    @app.post("/clean_data")
    async def data_request(request: Request) -> str:
        start_time: float = time.time()
        request_object: RequestObject = RequestObject(request)
        data_cleaner: DataCleaner = DataCleaner(request_object)
        paths = await data_cleaner.clean_data()
        total_time = (time.time() - start_time)
        LOGGER.logger.info("total time taken: %s seconds" % total_time)
        return json({"message": "successful", 
                     "paths": paths, 
                     "total time taken": total_time})
