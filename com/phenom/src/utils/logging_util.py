"""
Logger class
"""
import logging
import logging.config

from sanic import Sanic
import os, os.path
app: Sanic = Sanic.get_app("data_preparation")

class Logging:
    """
    Logging data
    """
    __instance = None

    @staticmethod
    def get_instance():
        """ Static access method. """
        if Logging.__instance is None:
            Logging()
        return Logging.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if Logging.__instance is not None:
            raise Exception("This class is a Logger!")
        else:
            logging.config.fileConfig(app.config.LOG_CONFIG_PATH)

            self.logger = logging.getLogger('fileLogger')
            self.log_err = logging.getLogger('errLogger')
            Logging.__instance = self