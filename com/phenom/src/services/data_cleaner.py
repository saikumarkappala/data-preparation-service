import os.path
from com.phenom.src.connections.mongodbClient import MongodbClient
from com.phenom.src.connections.ongdbClient import OngdbClient
from sanic import Sanic
from csv import writer
import pandas as pd
import pymongo
import time
from com.phenom.src.utils.logging_util import Logging
from com.phenom.src.singletons.request_object import RequestObject

LOGGER = Logging.get_instance()
app = Sanic.get_app("data_preparation")


class DataCleaner:
    def __init__(self, request):
        self.__mongodbClient: MongodbClient = MongodbClient(mongo_URL = app.config.MONGOPROD_URL_JOBS)
        self.__candidatesmongodbClient: MongodbClient = MongodbClient(mongo_URL = app.config.MONGOPROD_URL_CANDIDATES)
        self.__request = request
        self.__ongdbClientService = OngdbClient()

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value: RequestObject) -> None:
        self.__request: RequestObject = value

    @property
    def mongodbClient(self):
        return self.__mongodbClient

    @mongodbClient.setter
    def mongodbClient(self, value: MongodbClient) -> None:
        self.__mongodbClient: MongodbClient = value

    @property
    def candidatesmongodbClient(self):
        return self.__candidatesmongodbClient

    @candidatesmongodbClient.setter
    def candidatesmongodbClient(self, value: MongodbClient) -> None:
        self.__candidatesmongodbClient: MongodbClient = value

    async def clean_data(self):
        data_fields = self.__request.dataFields
        paths = {}
        for i in data_fields:
            if i == "candidates":
                paths['candidates_data'] = await self.clean_candidates_data()
            if i == "jobs":
                paths['jobs_data'] = await self.clean_jobs_data()
        return paths

    def attach_data(self, data, case, param = None):
        if not case:
            if type(data) == str:
                return "_".join(data.split())
            elif type(data) == list:
                text = ""
                for j in data:
                    text = text + " " + "_".join(j.split())
                return text
        else:
            if type(data) == dict:
                if param in data:
                    return "_".join(data[param].split())
            elif type(data) == list:
                text = ""
                for j in data:
                    if param in j:
                        text = text + " " + "_".join(j[param].split())
                return text

    def process_query(self, user_id, query):
        try:
            query: str = query.replace("REFNUM",self.__request.refNum)
            parameters = {}
            parameters["CANDIDATEID"] = user_id

            category : list = self.__ongdbClientService.query(query,param=parameters)
            return category
        except Exception as e:
            LOGGER.logger.error(e)

    async def clean_jobs_data(self):
        jobsCollection = self.__mongodbClient.get_mongo_collection(app.config.DB_NAME, app.config.JOBS_COLLECTION)

        query = {"ml_skills.0": {"$exists": True},
                 "refNum": self.__request.refNum,
                 "locale": {"$regex": "^en"}}
        # important columns in jobs data: ml_title, ml_skills, refNum, locale, jobId,
        # ml_job_parser.skills_importance.skills, ml_job_parser.ml_job_cards.education
        #ml_job_parser.ml_job_cards.experience

        jobs_fields = {}
        jobs_fields["jobId"] = 1
        jobs_fields[self.__request.jobsLabel] = 1
        for i in self.__request.jobsFields:
            jobs_fields[i] = 1

        projection = jobs_fields

        start_time = time.time()
        mongo_cursor = jobsCollection.find(query, projection)
        LOGGER.logger.info("total time taken to return cursor: %s seconds" % (time.time() - start_time))
        data_sample = None
        try:
            data_sample = mongo_cursor.next()
        except Exception as e:
            LOGGER.logger.exception(e)

        start_time = time.time()
        while data_sample is not None:
            try:
                job_id = data_sample["jobId"]
                category = data_sample[self.__request.jobsLabel]

                text = " "
                for i in self.__request.jobsFields:
                    data = " "
                    if i.count('.') == 0:
                        if i in data_sample:
                            data = self.attach_data(data_sample[i], False)
                    else:
                        arr = i.split('.')
                        real_data = data_sample
                        for j in arr[:-1]:
                            if j in real_data:
                                real_data = real_data[j]
                        try:
                            data = self.attach_data(real_data, True, arr[-1])
                        except Exception as e:
                            LOGGER.logger.exception(e)

                    try:
                        text = text + " " + data
                    except Exception as e:
                        LOGGER.logger.exception(i+"is empty")

                with open('jobs_data_'+self.__request.refNum+'.csv', 'a') as f_object:
                    writer_object = writer(f_object)
                    text = " ".join(text.lower().split())
                    if text != "":
                        final_text = "__label__" + "_".join(category.lower().split()) + " " + text
                        writer_object.writerow([job_id, final_text])
                    f_object.close()

            except Exception as e:
                LOGGER.logger.exception(e)

            try:
                data_sample = mongo_cursor.next()
            except StopIteration:
                LOGGER.logger.info("total time taken to prepare data: %s seconds" % (time.time() - start_time))
                break
        return os.path.abspath('jobs_data_'+self.__request.refNum+'.csv')
    
    def to_set(self, data_sample, result_set, user_id):
        df = list(pd.read_csv('/home/saikumarkappala/PycharmProjects/data-preparation-for-embedding-model-training/candidates_data_ANCEUS.csv', header=None).iloc[:,0])

        try:
            sample = data_sample.next()
        except StopIteration:
            return result_set

        while sample is not None:
            try:
                if sample[user_id] not in df:
                    result_set.add(sample[user_id])
            except Exception as e:
                LOGGER.logger.exception(e)
            try:
                sample = data_sample.next()
            except StopIteration:
                break
        return result_set

    async def clean_candidates_data(self):
        db_name = str("ETG_"+str(self.__request.refNum))
        candidatesCollection = self.__candidatesmongodbClient.get_mongo_collection(db_name, app.config.CANDIDATES_COLLECTION)

        result_set = set()
        try:
            start_time = time.time()
            query_ids = {"applied.0": {"$exists": True}}
            projection_ids = {"userId": 1}
            unique_cand_ids_1 = self.__candidatesmongodbClient.get_mongo_collection(db_name, app.config.APPLICATIONS_COLLECTION_1).find(query_ids, projection_ids)#.aggregate([{"$match": {"applied.0": {"$exists": True}}}, {"$group": {"_id": "$userId"}}])#, allowDiskUse=True)
            unique_cand_ids_2 = self.__candidatesmongodbClient.get_mongo_collection(db_name, app.config.APPLICATIONS_COLLECTION_2).find(query_ids, projection_ids)#.aggregate([{"$match": {"applied.0": {"$exists": True}}}, {"$group": {"_id": "$userId"}}])#, allowDiskUse=True)
            unique_cand_ids_3 = self.__candidatesmongodbClient.get_mongo_collection(db_name, app.config.APPLICATIONS_COLLECTION_3).find(projection={"candidateId":1})#.aggregate([{"$group": {"_id": "$candidateId"}}])#, allowDiskUse=True)
            LOGGER.logger.info("time taken for aggregation: %s seconds" % (time.time() - start_time))
            result_set = self.to_set(unique_cand_ids_1, result_set, "userId")
            print(len(result_set))
            result_set = self.to_set(unique_cand_ids_2, result_set, "userId")
            print(len(result_set))
            result_set = self.to_set(unique_cand_ids_3, result_set, "candidateId")
            print(len(result_set))
            LOGGER.logger.info("Total candidates having applications: %s" % (len(result_set)))

        except Exception as e:
            LOGGER.logger.exception(e)

        #important columns in candidates data: userId, title or designation(mostly empty),
        # education is list of dicts,education.degree, education.fieldOfStudy,
        # experience is list of dicts, experience.company, experience.title, experience.jobTitle(mostly available)
        # refNum, skills(mostly empty), skillList list of dicts, skillList.value

        candidates_fields = {}
        candidates_fields["userId"] = 1
        for i in self.__request.candidatesFields:
            candidates_fields[i] = 1

        projection = candidates_fields
        list1 = list(result_set)
        n = 5
        lists = [list1[i:i + n] for i in range(0, len(list1), n)]

        for curr_list in lists:
            start_time = time.time()
            query = {"userId": {"$in": curr_list}}
            mongo_cursor = candidatesCollection.find(query, projection)
            data_sample = None
            try:
                data_sample = mongo_cursor.next()
            except StopIteration as e:
                LOGGER.logger.exception(e)

            while data_sample is not None:
                try:
                    text = ""
                    user_id = data_sample["userId"]
                    for i in self.__request.candidatesFields:
                        data = ""
                        if i.count('.') == 0:
                            if i in data_sample:
                                data = self.attach_data(data_sample[i], False)
                        else:
                            arr = i.split('.')
                            real_data = data_sample
                            for j in arr[:-1]:
                                if j in real_data:
                                    real_data = real_data[j]
                            try:
                                data = self.attach_data(real_data, True, arr[-1])
                            except Exception as e:
                                LOGGER.logger.exception(e)
                        text = text + " " + data

                    category = self.process_query(user_id, app.config.CANDIDATEID_TO_CATEGORY)
                    with open('candidates_data_'+self.__request.refNum+'.csv', 'a') as f_object:
                        writer_object = writer(f_object)
                        text = " ".join(text.lower().split())
                        if text != "" and len(category)>0:
                            label_text = ""
                            for i in category:
                                label_text = label_text + "__label__" + "_".join(i.lower().split()) + " "
                            writer_object.writerow([user_id, label_text + text])
                        f_object.close()
                except Exception as e:
                    LOGGER.logger.exception(e)

                try:
                    data_sample = mongo_cursor.next()
                except StopIteration:
                    LOGGER.logger.info("total time taken to prepare data: %s seconds" % (time.time() - start_time))
                    break

        return os.path.abspath('candidates_data_' + self.__request.refNum + '.csv')








    async def clean_candidates_data_dup(self):

        '''group_query = {"originalId": {"$first": '$_id'},"_id": '$jobId',"category":{"$first":"$category"}}
              project_query = {"_id": '$originalId', "jobId": '$_id', "category":1}
              for i in self.__request.candidatesFields:
                  group_query[i.split('.')[0]] = {"$first":"$"+i.split('.')[0]}
                  project_query[i.split('.')[0]] = 1
              mongo_cursor = jobsCollection.aggregate([{"$match":query},{"$group":group_query},{"$project":project_query}],allowDiskUse=True)'''

        '''if arr[0] in data_sample:
                                    if len(arr) == 2:
                                        real_data = data_sample[arr[0]]
                                        data = self.attach_data(real_data, True, arr[1])

                                    elif len(arr) == 3:
                                        if arr[1] in data_sample[arr[0]]:
                                            real_data = data_sample[arr[0]][arr[1]]
                                            data = self.attach_data(real_data, True, arr[2])'''

        candidatesCollection = self.__candidatesmongodbClient.get_mongo_collection(str("ETG_"+str(self.__request.refNum)), app.config.CANDIDATES_COLLECTION)

        query = { #"skillList.0": {"$exists": True},
                 "refNum": self.__request.refNum}

        unique_cand_ids_1 = self.__candidatesmongodbClient.get_mongo_collection(str("ETG_"+str(self.__request.refNum)), app.config.APPLICATIONS_COLLECTION_1).distinct("candidateId", {"refNum": self.__request.refNum, "applied": {"$gt": 0}})
        unique_cand_ids_2 = self.__candidatesmongodbClient.get_mongo_collection(str("ETG_" + str(self.__request.refNum)), app.config.APPLICATIONS_COLLECTION_2).distinct("candidateId", {"refNum": self.__request.refNum, "applied": {"$gt": 0}})
        unique_cand_ids_3 = self.__candidatesmongodbClient.get_mongo_collection(str("ETG_" + str(self.__request.refNum)), app.config.APPLICATIONS_COLLECTION_3).distinct("candidateId", {"refNum": self.__request.refNum})
        s1 = set(unique_cand_ids_1)
        s2 = set(unique_cand_ids_2)
        s3 = set(unique_cand_ids_3)
        set1 = s1.intersection(s2)
        result_set = set1.intersection(s3)
        unique_cand_ids = list(result_set)

        #important columns in candidates data: userId, title or designation(mostly empty),
        # education is list of dicts,education.degree, education.fieldOfStudy,
        # experience is list of dicts, experience.company, experience.title, experience.jobTitle(mostly available)
        # refNum, skills(mostly empty), skillList list of dicts, skillList.value

        candidates_fields = {}
        candidates_fields["userId"] = 1
        for i in self.__request.candidatesFields:
            candidates_fields[i] = 1

        projection = candidates_fields

        start_time = time.time()
        #mongo_cursor = candidatesCollection.find(query, projection)
        mongo_cursor = candidatesCollection.aggregate([{"$match": {"refNum": self.__request.refNum}}, {"$project": projection}, {"$limit":100}])
        LOGGER.logger.info("total time taken to return cursor: %s seconds" % (time.time() - start_time))
        data_sample = None
        try:
            data_sample = mongo_cursor.next()
        except Exception as e:
            LOGGER.logger.exception(e)

        start_time = time.time()
        while data_sample is not None:
            try:
                user_id = data_sample["userId"]
                category = self.process_query(user_id, app.config.CANDIDATEID_TO_CATEGORY)
                print(category)

                text = ""
                for i in self.__request.candidatesFields:
                    data = ""
                    if i.count('.') == 0:
                        if i in data_sample:
                            data = self.attach_data(data_sample[i], False)
                    else:
                        arr = i.split('.')
                        if arr[0] in data_sample:
                            if len(arr) == 2:
                                real_data = data_sample[arr[0]]
                                data = self.attach_data(real_data, True, arr[1])

                            elif len(arr) == 3:
                                if arr[1] in data_sample[arr[0]]:
                                    real_data = data_sample[arr[0]][arr[1]]
                                    data = self.attach_data(real_data, True, arr[2])
                    text = text + " " + data

                with open('candidates_data_'+self.__request.refNum+'.csv', 'a') as f_object:
                    writer_object = writer(f_object)
                    text = " ".join(text.lower().split())
                    if text != "":
                        writer_object.writerow([user_id, text])
                    f_object.close()

            except Exception as e:
                LOGGER.logger.exception(e)

            try:
                data_sample = mongo_cursor.next()
            except StopIteration:
                LOGGER.logger.info("total time taken to prepare data: %s seconds" % (time.time() - start_time))
                break
        return os.path.abspath('candidates_data_'+self.__request.refNum+'.csv')

