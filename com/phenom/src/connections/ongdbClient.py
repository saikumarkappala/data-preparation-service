from neo4j import GraphDatabase
from sanic import Sanic

from com.phenom.src.utils.logging_util import Logging

app: Sanic = Sanic.get_app("data_preparation")
LOGGER = Logging.get_instance()


class OngdbClient:
    def __init__(self):
        self.__uri = app.config.ONGDB_URI
        self.__user = app.config.ONGDB_USER
        self.__pwd = app.config.ONGDB_PASSWORD
        self.__client = None
        try:
            self.__client = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd))
        except Exception as e:
            LOGGER.logger.error(e)

    @property
    def client(self):
        return self.__client

    @client.setter
    def client(self, value):
        try:
            self.__client = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd))
            print("m")
        except Exception as e:
            LOGGER.logger.error(e)

    def close(self):
        if self.__client is not None:
            self.__client.close()

    def query(self, query, param=None, db=None):
        assert self.__client is not None, "Driver not initialized!"
        session = None
        response = None
        try:
            session = self.__client.session(database=db) if db is not None else self.__client.session()
            result = list(session.run(query, parameters=param))
            result_list = list()
            for val in result:
                result_list.append(val[0])
            response = result_list
        except Exception as e:
            LOGGER.logger.error("Query failed:", e)
        finally:
            if session is not None:
                session.close()
        return response
