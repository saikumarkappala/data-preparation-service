import pymongo
from pymongo import MongoClient
from typing import Optional
from sanic import Sanic
import asyncio
import motor.motor_asyncio
from motor.core import AgnosticCollection
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorDatabase
from com.phenom.src.utils.logging_util import Logging

LOGGER = Logging.get_instance()
app: Sanic = Sanic.get_app("data_preparation")


class MongodbClient:

    def __init__(self,
                 DB_HOST: Optional[str]=None,
                 DB_NAME: Optional[str]=None,
                 DB_USER: Optional[str]=None,
                 mongo_URL:  str=None,
                 ):
        self.mongo_url = mongo_URL
        try:
            self.__client: MongoClient = pymongo.MongoClient(self.mongo_url)
        except Exception as e:
            LOGGER.logger.exception(e)
    
    def get_mongo_db(self, db_name:str):
        try:
            db = self.__client[db_name]
            return db
        except Exception as e:
            LOGGER.logger.exception(e)

    def get_mongo_collection(self,db_name:str, collection_name:str):
        try:
            db = self.__client[db_name]
            collection = db[collection_name]
            return collection
        except Exception as e:
            LOGGER.logger.exception(e)
