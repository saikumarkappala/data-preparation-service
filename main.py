from sanic import Sanic
from sanic.request import Request
from sanic.response import text


app = Sanic("data_preparation")
app.update_config("/home/saikumarkappala/PycharmProjects/data-preparation-for-embedding-model-training/conf/application.properties")
# below import should be after intializing Sanic
from com.phenom.src.utils.logging_util import Logging
LOGGER = Logging.get_instance()

# Controller classes should be imported here to register the handlers
from com.phenom.src.controllers.DataPreparationController import DataPreparationController

@app.get("/testing")
async def foo_handler(request: Request) -> str:
    LOGGER.logger.info("Service is up and running")
    return text("Service is up and running")

# Press the green button in the gutter to run the script.
if __name__ == "__main__":
    LOGGER.logger.info("Starting the experience-prediction web service.")
    print("Starting the experience-prediction web service.")
    app.run(host=app.config.SOCKET_HOST, port=app.config.PORT, workers=app.config.THREAD_POOL)
